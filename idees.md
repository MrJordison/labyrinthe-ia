Organisation proposées :

* Le problème est modélisé par une classe Probleme qui stocke :
  * Attributs :
    * L'état de départ
    * Le nombre de noeuds visités
  * Méthodes :
    * ajouterFils()
    * ajouterListeFils()
    * reconstruireChemin()
    * estBut()
    * rechercher()
* Un noeud est représenté par une classe Noeud:
  * Une Action
  * Le coût total depuit le début jusqu'à ce noeud
  * La profondeur de ce noeud dans l'arbre
  * Le noeud parent
  * L'état qui représente ce noeud
* L'action sera représentée par une classe Action:
  * Le mouvement qui correspond à l'action (D, G, H, B)
  * Le coût (1 normalement ?)
* Un Chemin qui représente un chemin depuis un noeud racine jusqu'au noeud courant:
  * Une liste de PaireActionEtat qui représente la route du début jusqu'à la fin
  * Le coût total du chemin
  * L'état à la tête du chemin
* L'état sera une classe Etat :
  * successeurs() -> renvoie la liste des enfants de cet état (liste de PaireActionEtat)
* Une PaireActionEtat est...une paire constituée d'un état et d'une action (shocking!)

C'est une adaptation de ce qu'on avait fait l'année dernière en IA.