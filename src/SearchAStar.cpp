#include "include/SearchAStar.hpp"

SearchAStar::SearchAStar(State &start, Labyrinthe *laby)
    : SearchProblem(start, laby) {
}

void SearchAStar::addChild(std::vector<Node> &liste, Node childNode) {
    if (SearchProblem::stateValid(childNode.getState())) {
	int h = SearchProblem::heuristique(childNode.getState());
	int g = childNode.getCost();
	int f = g + h;

	int idx = 0;
	for (idx=0; idx < liste.size(); idx++) {
	    int hCourant = SearchProblem::heuristique(liste.at(idx).getState());
	    int gCourant = liste.at(idx).getCost();
	    int fCourant = gCourant + hCourant;

	    if (fCourant > f) {
		break;
	    }
	}

	liste.insert(liste.begin() + idx, childNode);
    }
}
