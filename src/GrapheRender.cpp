#include "include/GrapheRender.hpp"
#include <thread>
#include <chrono>
#include <future>
#include <ctime>

using namespace std;

int get_time_difference_ms(std::clock_t start, std::clock_t end) {
    return 1000 * (end - start) / CLOCKS_PER_SEC;
}

GrapheRender::GrapheRender(SearchProblem * graphe){
    this->graphe = graphe;
    positions_visited = new vector<vec2i>();
    path_render = nullptr;
    generated_path = nullptr;
    sprite = nullptr;
    cpt = 0;
    speed=1;
    upscale = 1.f;
}

GrapheRender::~GrapheRender(){
    delete positions_visited;
    delete graphe;
    delete sprite;
}

void GrapheRender::run_threaded_search(){
    std::clock_t start, end;
    start = std::clock();
    generated_path = graphe->search();
    end = std::clock();

    std::cout << "Temps de recherche : " << get_time_difference_ms(start, end) << " ms" << std::endl;
    std::cout << "Nombre de noeuds visités : " << graphe->getNodesVisited() << std::endl;
    std::cout << "Longueur du chemin : " << generated_path->getCost() << std::endl;
}

void GrapheRender::maj_compteur(){
    this_thread::sleep_for(chrono::milliseconds(2000/speed));
        cpt++;
    if(generated_path == nullptr || (generated_path != nullptr && cpt < positions_visited->size())){
        thread bla{&GrapheRender::maj_compteur,this};
        bla.detach();
    }
    else if(generated_path != nullptr && cpt >= positions_visited->size())
        path_render->set_path(generated_path);
}


void GrapheRender::init(){
    graphe->setNodeCallback(&updateGrapheParcouru, this);
    thread t(&GrapheRender::run_threaded_search,this);
    t.detach();


    thread printLater{&GrapheRender::maj_compteur,this};
    printLater.detach();
}

void GrapheRender::render(sf::RenderWindow& window){
    if(positions_visited!=nullptr){
        vector<vec2i>::iterator i;
        int nb_displayed = 0;
        for(i = positions_visited->begin()  ; i != positions_visited->end(); ++i){
            if(nb_displayed >= cpt){
                break;
            }
            if(sprite!=nullptr){
                sprite->setPosition((*i).x*(32*upscale),(*i).y*(32*upscale));
                window.draw(*sprite);
            }
            nb_displayed++;

        }
    }
}

void GrapheRender::update(){
        
}

bool GrapheRender::setTexture(std::string filename){
    sf::Texture *  texture = new sf::Texture();
    if(!texture->loadFromFile(filename,sf::IntRect(0,0,32,32))){
        delete texture;
        return false;
    }
    sprite = new sf::Sprite();
    sprite->setTexture(*texture);
    sprite->setColor(sf::Color(255,255,255,128));
}

void GrapheRender::add_visited_position(vec2i position){
    positions_visited->push_back(position);
}

void GrapheRender::bind_path_rendering(PathRender * path_render){
    this->path_render = path_render;   
}

int GrapheRender::get_speed() const{return speed;}

void GrapheRender::set_speed(int speed){this->speed = speed;}

float GrapheRender::get_upscale() const{return upscale;}

void GrapheRender::set_upscale(float u){
    sprite->setScale((1.f/upscale)*u,(1.f/upscale)*u);
    this->upscale = u;
}

void updateGrapheParcouru(GrapheRender * g_render, Node new_noeud){
   g_render->add_visited_position(new_noeud.getState().getPosition());     
}

