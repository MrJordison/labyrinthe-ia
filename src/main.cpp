#include "include/Labyrinthe.hpp"
#include "include/SearchBestFirst.hpp"
#include "include/SearchUniformCost.hpp"
#include "include/SearchAStar.hpp"
#include "include/State.hpp"
#include "include/EngineRender.hpp"
#include "include/LabyrintheRender.hpp"
#include "include/Path.hpp"
#include "include/GrapheRender.hpp"

#include <iostream>
#include <string>
#include <cstdlib>
#include <SFML/Graphics.hpp>

using namespace std;

int main(int argc, char* argv[]){
  try{
        Labyrinthe * l;
        SearchProblem * ia;

	string labyrinthe_filename;
	int type_ia;
	int vitesse_affichage;
	int type_heuristique;
	
        
        //1er argument : nom du fichier ou est stocké le labyrinthe
        if(argc > 1){
	    labyrinthe_filename = string(argv[1]);
        }
        else
            labyrinthe_filename = "../resources/labyrinthes/lab_1.csv";

	l = new Labyrinthe(Labyrinthe::importProf(labyrinthe_filename));

        State start = State(l->get_start_pos());
        /*2e argument : type d'IA à utiliser
        *   0 : BestFirst
        *   1 : UniformCost
        *   2 : A*
        */
        if(argc > 2){
	    type_ia = atoi(argv[2]);
        }
        else {
	    cout << "Quelle IA voulez-vous utiliser ?" << endl;
	    cout << "0. Meilleur d'Abord" << endl;
	    cout << "1. Coût Uniforme" << endl;
	    cout << "2. A*" << endl;
	    cout << "Choix : ";
	    cin >> type_ia;
	}

	switch(type_ia){
	case 0 : 
	    ia = new SearchBestFirst(start,l);
	    break;
	case 1 :
	    ia = new SearchUniformCost(start,l);
	    break;
	case 2 :
	    ia = new SearchAStar(start,l);
	    break;
	default:
	    cout << "Choix invalide" << endl;
	    exit(1);
	    break;
	}

        //3e argument : vitesse d'affichage du labyrinthe visité et du chemin trouvé(max value : 500)
        if(argc > 3){
            vitesse_affichage = atoi(argv[3]);
        } else {
	    vitesse_affichage = 100;
	}

	// 4ème argument : heuristique à utiliser
	if (argc > 4) {
	    type_heuristique = atoi(argv[4]);
	} else {
	    cout << "Type d'heuristique à utiliser" << endl;
	    cout << "1. Distance de Manhattan" << endl;
	    cout << "2. Nombre de cases" << endl;
	    cout << "Choix : ";
	    cin >> type_heuristique;
	}

	switch (type_heuristique) {
	case 1: 
	case 2:
	    ia->setTypeHeuristique(type_heuristique);
	    break;
	default:
	    cout << "Choix invalide" << endl;
	    exit(1);
	    break;
	}

        
        int width = 1300;
        int height = 800;
        bool stop = false;
        float upscale = 1.;
        vec2i dims = l->get_map_dims();
        while(!stop){
            if(dims.y*32*upscale > height || dims.x*32*upscale > width)
                upscale /= 2.;
            else
                stop = true;
        }

        EngineRender * engine = new EngineRender();

        LabyrintheRender * render_laby = new LabyrintheRender(l);
        render_laby->setTexture(-1,"../resources/assets/trapdoor.png");
        render_laby->setTexture(0,"../resources/assets/dirt.png");
        render_laby->setTexture(1,"../resources/assets/stonebrick_mossy.png");
        render_laby->setTexture(2,"../resources/assets/enchanting_table_top.png");


        GrapheRender * g_render = new GrapheRender(ia);
        g_render->setTexture("../resources/assets/red_sand.png");

        PathRender * path_render = new PathRender(nullptr);
        path_render->setTexture("../resources/assets/sand.png");

        g_render->bind_path_rendering(path_render);

        g_render->set_upscale(upscale);
        render_laby->set_upscale(upscale);
        path_render->set_upscale(upscale);

        g_render->set_speed(vitesse_affichage);
        path_render->set_speed(vitesse_affichage);

        engine->add_object(render_laby);
        engine->add_object(g_render);
        engine->add_object(path_render);

        engine->init();

        sf::RenderWindow window(sf::VideoMode(dims.x*upscale*32,dims.y*upscale*32), "Pathfinder");
        // run the program as long as the window is open
        while (window.isOpen()){
            // check all the window's events that were triggered since the last iteration of the loop


            sf::Event event;
            while (window.pollEvent(event)){
                // "close requested" event: we close the window
                if (event.type == sf::Event::Closed)
                    window.close();
            }

            engine->render(window);

        }
    }
    catch(string const& s){
	cerr << s;
    }

    return 0;
}
