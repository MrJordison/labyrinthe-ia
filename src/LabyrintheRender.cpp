#include "include/LabyrintheRender.hpp"

using namespace std;


LabyrintheRender::LabyrintheRender(Labyrinthe * laby){
    model_laby = laby ; 
    textures = new map<int,sf::Texture*>();
    sprites = new map<int,sf::Sprite*>();
    upscale = 1.f;

    //initialise les emplacements de textures à nullptr
    textures->emplace(-1,nullptr);
    textures->emplace(0,nullptr);
    textures->emplace(1,nullptr);
    textures->emplace(2,nullptr);
    sprites->emplace(-1,nullptr);
    sprites->emplace(0,nullptr);
    sprites->emplace(1,nullptr);
    sprites->emplace(2,nullptr);
}

LabyrintheRender::~LabyrintheRender(){
    //delete l'ensemble des textures et de sprites
    //et vérifie si elles existent avant
    for(auto& sprite : *sprites)
        if(sprite.second != nullptr)
            delete sprite.second;
    for(auto& texture : *textures)
        if(texture.second != nullptr)
            delete texture.second;

    delete sprites;
    delete textures;
}

bool LabyrintheRender::setTexture(int value, std::string filename){
    //tente de créer la texture à partir du fichier
    sf::Texture* newtex = new sf::Texture();

    //si le fichier est introuvable retourne false
    if(!newtex->loadFromFile(filename,sf::IntRect(0,0,32,32))){
        delete newtex;
        return false;
    }

    //crée un sprite avec la nouvelle texture
    sf::Sprite* newsprite = new sf::Sprite();
    newsprite->setTexture(*newtex);

    //Si il y a déjà une texture, la supprime avant d'ajouter la nouvelle
    if(textures->at(value) != nullptr){
        delete textures->at(value);
        textures->at(value) = nullptr;
    }
    textures->at(value) = newtex;

    //Si il y a déjà un sprite, le supprime avant d'ajouter le nouveau
    if(sprites->at(value) != nullptr){
        delete sprites->at(value);
        sprites->at(value) = nullptr;
    }
    sprites->at(value) = newsprite;
    
    //applique l'upscale actuel sur le sprite avant de retourner true
    sprites->at(value)->setScale(32*upscale,32*upscale);
    return true;

}

void LabyrintheRender::init(){
     
}

void LabyrintheRender::render(sf::RenderWindow& window){
    sf::Sprite* sprite = nullptr;

    //Parcours de l'ensemble de la map
    for(int x = 0 ; x < model_laby->get_map_dims().x ; x++)
        for(int y = 0; y < model_laby->get_map_dims().y ; ++y){
            sprite = nullptr;

            //Cas spécial de la sortie où il y a deux textures dont celle du couloir
            if(model_laby->get_value(vec2i(x,y))==-1){
                sprite = sprites->at(0);

                //vérifie si le sprite/texture est existant et affiche si oui
                if(sprite != nullptr){
                    sprite->setPosition(sf::Vector2f(upscale*32.*x,upscale*32.*y));
                    window.draw(*sprite);
                    sprite = nullptr;
                }
            }

            //récupère le sprite en fonction de la valeur à l'emplacement(x,y)
            //de la map
            sprite = sprites->at(model_laby->get_value(vec2i(x,y)));

            //vérifie si le sprite/texture est existant et affiche si oui
            if(sprite != nullptr){
                sprite->setPosition(sf::Vector2f(upscale*32.*x,upscale*32.*y));
                window.draw(*sprite);
            }
        }
}

void LabyrintheRender::update(){
    
}

float LabyrintheRender::get_upscale() const{return upscale;}

void LabyrintheRender::set_upscale(float u){

    //pour chaque sprite/texture, le remet à sa taille initiale puis applique le nouveau facteur d'upscale
    for(map<int,sf::Sprite*>::iterator it = sprites->begin(); it!=sprites->end();++it)
        if(it->second != nullptr)
            it->second->setScale((1.f/upscale)*u,(1.f/upscale)*u);

    //stocke le nouveau coefficient
    this->upscale = u;
}


