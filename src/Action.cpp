#include "include/Action.hpp"

Action::Action() {
    direction = Direction::HAUT;
    cout = 0;
}

Action::Action(Direction dir, float c) {
    this->direction = dir;
    this->cout = c;
}

Action::Action(const Action &copy) {
    direction = copy.getDirection();
    cout = copy.getCout();
}

Direction Action::getDirection() const {
    return direction;
}

float Action::getCout() const {
    return cout;
}

std::ostream& operator<<(std::ostream& os, const Action& action) {
    os << "Aller ";
    switch (action.direction) {
    case Direction::HAUT:
	os << "en HAUT";
	break;
    case Direction::BAS:
	os << "en BAS";
	break;
    case Direction::DROITE:
	os << "à DROITE";
	break;
    case Direction::GAUCHE:
	os << "à GAUCHE";
	break;
    }

    os << " (coût " << action.cout << ")";
    return os;
}
