#include "include/Path.hpp"

Path::Path() {
    cost = 0.0;
}

Path::Path(State head, double cost) {
    this->head = State(head.getPosition());
    this->cost = cost;
}

void Path::addStep(std::pair<Action, State> pair) {
    liste.push_back(pair);
}

int Path::getCost() const { return cost; }

State Path::getHead() const { return head; }

std::vector<std::pair<Action, State> > Path::getListe() const { return liste; }

std::ostream& operator<<(std::ostream& os, const Path& path) {
    for (auto const& pair : path.getListe()) {
	os << pair.first << " pour aller en " << pair.second.getPosition().x << ";" << pair.second.getPosition().y << std::endl;
    }

    return os;
}
