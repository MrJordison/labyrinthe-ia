#include "include/SearchBestFirst.hpp"

SearchBestFirst::SearchBestFirst(State &start, Labyrinthe *laby)
    : SearchProblem::SearchProblem(start, laby) {
}

void SearchBestFirst::addChild(std::vector<Node> &liste, Node childNode) {
    if (SearchProblem::stateValid(childNode.getState())) {
	int h = SearchProblem::heuristique(childNode.getState());

	int idx = 0;
	for(idx = 0; idx < liste.size(); idx++) {
	    int hCourant = SearchProblem::heuristique(liste.at(idx).getState());
	    if (hCourant > h) {
		break;
	    }
	}

	liste.insert(liste.begin() + idx, childNode);
    }
}
