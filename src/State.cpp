#include "include/State.hpp"

State::State() {
    position = vec2i(0, 0);
}

State::State(vec2i pos) {
    position = vec2i(pos.x, pos.y);
}

State::State(const State &copy) {
    position = vec2i(copy.getPosition().x, copy.getPosition().y);
}

bool operator==(const State& first, const State& second) {
    return first.getPosition().x == second.getPosition().x &&
	first.getPosition().y == second.getPosition().y;
}

std::vector<std::pair<Action, State> > State::successors() {
    std::vector<std::pair<Action, State> > result;
    vec2i haut = vec2i(0, -1),
	bas = vec2i(0, 1),
	gauche = vec2i(-1, 0),
	droite = vec2i(1, 0);
    
    result.push_back(std::make_pair(Action(Direction::HAUT, 1),
				    State(position + haut)));

    result.push_back(std::make_pair(Action(Direction::BAS, 1),
				    State(position + bas)));

    result.push_back(std::make_pair(Action(Direction::GAUCHE, 1),
				    State(position + gauche)));

    result.push_back(std::make_pair(Action(Direction::DROITE, 1),
				    State(position + droite)));
    
    return result;
}

vec2i State::getPosition() const {
    return position;
}
