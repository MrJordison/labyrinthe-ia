#pragma once

#include <vector>
#include <utility>

#include "Action.hpp"
#include "vec2i.hpp"
#include "Labyrinthe.hpp"
#include "Direction.hpp"

class State{
private:
    /**
     * Position actuelle du joueur
     */
    vec2i position;
    
public:
    State();
    State(vec2i pos);
    State(const State &copy);

    vec2i getPosition() const;

    friend bool operator==(const State &first, const State &second);
    
    /*****************/
    /* Méthodes      */
    /*****************/
    
    std::vector<std::pair<Action,State> > successors();


};
