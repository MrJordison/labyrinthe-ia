#pragma once

#include "State.hpp"
#include "Action.hpp"

/**
 * Représente un noeud dans l'algorithme de recherche. On garde un pointeur vers le noeud parent pour pouvoir remonter
 * au début quand on trouve une solution. L'attribut de coût nous permet d'optimiser sur le coût total depuis le début.
 */
class Node {
private:
  /**
   * L'action qui nous amène du noeud parent à celui-ci
   */
  Action action;

  /**
   * Le coût total depuis la racine de l'arbre jusqu'au noeud courant
   */
  double cost;

  /**
   * La profondeur actuelle dans le graphe (utile si on veut la limiter)
   */
  int depth;

  /**
   * Un pointeur vers le noeud parent
   */
  Node *parent;

  /**
   * L'état représenté par ce noeud
   */
  State state;

public:
  /**
   * Constructeur qui crée un objet Node
   */
  Node(State state, Node *parent, Action action, double cost, int depth);

    Node(const Node &copy);

  /****************************
   *    GETTERS & SETTERS     *
   ****************************/

  int getCost() const;
  int getDepth() const;

    Action getAction() const;
  Node* getParent() const;
  State getState() const;
};
