#pragma once

#include <vector>
#include <utility>
#include "State.hpp"
#include "Action.hpp"

/**
 * Classe représentant un chemin depuis la racine de l'arbre jusqu'à un noeud.
 * Représenté par une liste de paires d'Action et de States qui transforment le noeud racine en le noeud courant.
 * Un Path est le résultat retourné par search()
 */
class Path {
private:
    /**
     * Le coût depuis la racine de l'arbre jusqu'au noeud courant
     */
    double cost;

    /**
     * L'état racine de l'arbre
     */
    State head;

    /**
     * La liste de paires d'Action-State qui permet d'arriver au noeud courant depuis la racine
     */
    std::vector<std::pair<Action, State> > liste;
  
public:
    /**
     * Crée un Path vide avec un coût de 0
     */
    Path();
    Path(State head, double cost);

    /**
     * Ajoute une paire au chemin
     */
    void addStep(std::pair<Action, State> pair);
    
    int getCost() const;
    State getHead() const;
    std::vector<std::pair<Action, State> > getListe() const;

    friend std::ostream& operator<<(std::ostream& os, const Path& path);
};
