#pragma once

#include <SFML/Graphics.hpp>

class ObjectRender{
    
    public:

        /**
        *   Constructeur par défaut d'ObjectRender
        */
        ObjectRender(){}
        
        /**
        *   Constructeur par recopie d'ObjectRender, prend en paramètre un référence constance d'un ObjectRender
        *   à copier
        *   @param object l'instance d'ObjectRender à copier
        */
        ObjectRender(const ObjectRender& object){}

        /**
        *   Fonction permettant à un élément de l'interface
        *   d'initialiser ses différents attributs (textures, effets d'animations, etc...)
        */
        virtual void init(){}

        /**
        *   Fonction permettant à un élément de l'interface
        *   de s'afficher à l'écran via l'instance de RenderWindow passée en paramètre
        *   @param window la fenêtre ou sera affiché l'élément
        */
        virtual void render(sf::RenderWindow& window){}
        
        /**
        *   Fonction permettant à un élement de l'interface
        *   d'être modifiée entre deux passes de rendu (deux frames dessinées
        *   à l'écran (exemple modification d'animation selon le temps passé)
        */
        virtual void update(){}

};
