#pragma once

#include <vector>
#include <string>
#include <fstream>

class Importer{
    private : 
        std::string path;
        std::ifstream * file;


    public:
        //constructor
        Importer();
        Importer(std::string p);
        ~Importer();

        //getters, setters
        std::string get_path() const;
        void set_path(std::string);

        std::ifstream& get_file() const;

        //functions
        static std::vector<std::string> split(std::string, char delimiter);


    
};
