#pragma once

#include "ObjectRender.hpp"

#include <vector>

class EngineRender : public ObjectRender{
    
    private : 
        /**
        *   vecteur contenant l'ensemble des objets devant être rendu à l'écran
        *   l'ordre importe, les items seront rendus dans l'ordre de tri du vecteur
        *   les premiers seront affichés derrière les derniers objets qui seront devant
        */
        std::vector<ObjectRender*> * objects;

    public:

        /**
        *   Constructeur par défaut de la class EngineRender
        *   initialise l'attribut vecteur dans lequel on stockera les objets à rendre
        */
        EngineRender();

        /**
        *   Constructeur par recopie de la classe EngineRender prenant une autre instance d'EngineRender en paramètre
        *   @param render l'instance d'EngineRender
        */
        EngineRender(const EngineRender& render);

        /**
        *   Destructeur de la classe EngineRender
        */
        ~EngineRender();
        
        /**
        *   Fonction init, permet d'initialiser l'ensemble des ObjectsRender contenu dans l'EngineRender
        */
        virtual void init();

        /**
        *   Fonction qui appelle la fonction render de l'ensemble des ObjectsRender géré par l'EngineRender.
        *   @param window une instance de RenderWindow qui sera passée aux ObjectsRender afin qu'ils soient dessiné dans cette fenêtre
        */
        virtual void render(sf::RenderWindow& window);
    
        /**
        *   Fonction qui appelle la fonction update sur l'ensemble des ObjectsRender géré par l'EngineRender.
        */
        virtual void update();

        /**
        *   Getter retournant une référence constante sur la liste d'ObjectRender
        *   @return la référence constante donc non modifiable du vecteur d'ObjectRender
        */
        const std::vector<ObjectRender*>& get_objects() const;

        /**
        *   Fonction permettant d'ajouter un ObjectRender à l'affichage
        *   @param obj l'instance d'ObjectRender à ajouter à l'affichage
        */
        void add_object(ObjectRender* obj);

        /**
        *   Surcharge de l'opérateur =, renvoie un référence de l'instance d'EngineRender après copie des attributs
        *   @return la référence vers l'instance d'EngineRender modifiée
        */
        EngineRender& operator=(const EngineRender& render);
};
