#pragma once

#include <vector>
#include <utility>

#include "vec2i.hpp"
#include "Direction.hpp"


class Personnage{
    
    private:
        vec2i position;

    public:
        
        Personnage();

        std::vector<std::pair<vec2i,int> > get_vision();
        bool move(Direction dir);
};

