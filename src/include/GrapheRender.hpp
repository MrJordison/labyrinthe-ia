#pragma once

#include "ObjectRender.hpp"
#include "PathRender.hpp"
#include "SearchProblem.hpp"

#include <SFML/Graphics.hpp>
#include <vector>
#include <functional>

class SearchProblem;

class GrapheRender: public ObjectRender{
    
    private :
        SearchProblem * graphe;

        std::vector<vec2i> * positions_visited;

        sf::Sprite * sprite;

        int cpt;

        Path * generated_path;

        PathRender * path_render;        

        void run_threaded_search();

        void maj_compteur();

        int speed;

        float upscale;

    public : 

        GrapheRender(SearchProblem * graphe);

        ~GrapheRender();

        virtual void init();

        virtual void render(sf::RenderWindow& window);

        virtual void update();

        bool setTexture(std::string filename);

        void add_visited_position(vec2i position);

        void bind_path_rendering(PathRender * path_render);

        int get_speed() const;

        void set_speed(int speed);

        float get_upscale() const;

        void set_upscale(float u);

};

void updateGrapheParcouru(GrapheRender * g_render, Node new_noeud);

