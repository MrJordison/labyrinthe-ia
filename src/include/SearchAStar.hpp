#pragma once

#include "SearchProblem.hpp"

class SearchAStar : public SearchProblem {
public:
    SearchAStar(State &start, Labyrinthe *laby);
    void addChild(std::vector<Node> &liste, Node childNode);
};
