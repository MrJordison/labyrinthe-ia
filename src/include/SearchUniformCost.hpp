#pragma once

#include "SearchProblem.hpp"

class SearchUniformCost : public SearchProblem {
public:
    SearchUniformCost(State &start, Labyrinthe *laby);
    void addChild(std::vector<Node> &liste, Node childNode);
};
