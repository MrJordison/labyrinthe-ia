#pragma once

#include <iostream>

class vec2i{

    public:

        /*****************/
        /* Attributs     */
        /*****************/

        int x;
        int y;

        /*****************/
        /* Constructeurs */
        /*****************/

        /**
        *   Constructeur prenant en paramètre un entier x pour la coordonnée en abscisse et un entier y pour la coordonnée en ordonnée. Par défaut les valeurs sont initialisées à 0 .
        *   @x  l'entier correspondant à l'abscisse
        *   @y  l'entier correspondant à l'ordonnée
        */
        vec2i(int x = 0, int y = 0);

        /*****************/
        /* Méthodes      */
        /*****************/
        
        /** Surcharge de l'opérateur +, prend en paramètre deux vecteurs par référence. Retourne un nouveau vecteur représentant la somme des deux vecteurs
        *   @v  le premier vecteur
        *   @v2 le second vecteur
        */
        friend vec2i operator+(const vec2i& v, const vec2i& v2);

        /** Surcharge de l'opérateur -, prend en paramètre deux vecteurs par référence. Retourne un nouveau vecteur représentant la soustraction des deux vecteurs
        *   @v  le premier vecteur
        *   @v2 le second vecteur
        */
        friend vec2i operator-(const vec2i& v, const vec2i& v2);

        /** Surcharge de l'opérateur +=, prend en paramètre un vecteur par référence. Retourne un nouveau vecteur représentant la somme des deux vecteurs
        *   @v  le vecteur à additionner
        */
        vec2i operator+=(const vec2i& v);

        /** Surcharge de l'opérateur -=, prend en paramètre un vecteur par référence. Retourne un nouveau vecteur représentant la soustraction des deux vecteurs
        *   @v  le vecteur à soustraire
        */
        vec2i operator-=(const vec2i& v);

        /** Surcharge de l'opérateur ==, prend en paramètre deux vecteurs par référence. Compare leurs coordonnées x et y respectives et retourne un boolean à vrai si leurs coordonnées sont identiques, sinon retourne faux.
        *   @v  le premier vecteur
        *   @v2 le second vecteur
        */
        friend bool operator==(const vec2i& v, const vec2i& v2);

        friend bool operator<(const vec2i& v, const vec2i& v2);

        friend std::ostream& operator<<(std::ostream& os, const vec2i& v);
};
