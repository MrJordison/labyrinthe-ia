#pragma once

#include <cmath>
#include <functional>
#include "State.hpp"
#include "Action.hpp"
#include "Node.hpp"
#include "Path.hpp"
#include "GrapheRender.hpp"


class GrapheRender;

/**
 * Représente le problème de recherche de plus court chemin. 
 */
class SearchProblem {
protected:
    /**
     * Compte le nombre de noeuds visités depuis le début. Utile pour comparer les différentes heuristiques.
     */
    int nodesVisited;

    /**
     * Stocke les états noeuds visités, comme ça on évite de passer deux fois sur le même
     */
    std::vector<Node> alreadyVisited;

    /**
     * L'état initial du problème : le joueur est à la position de départ
     */
    State startState;

    /**
     * La labyrinthe dans lequel on travaille
     */
    Labyrinthe* laby;

    /**
     * Un pointeur vers une fonction qui sera appelée à chaque fois qu'on visite un noeud
     */
    std::function<void(GrapheRender*,Node)> func_visite_noeud;

    /**
    *   l'objet utilisé pour afficher les noeuds
    */
    GrapheRender* obj_render;

    /**
     * Le type d'heuristique à utiliser
     * 1 : distance de Manhattan
     * 2 : nombre de cases
     */
    int typeHeuristique;

public:
    /**
     * Constructeur qui crée un nouveau problème
     * @param start  L'état de départ
     */
    SearchProblem(State &start, Labyrinthe* laby);

    /**
     * Change la fonction callback
     */
    void setNodeCallback(std::function<void(GrapheRender*, Node)> func, GrapheRender* g_render);

    /**
     * Ajoute un enfant dans la liste donnée au bon endroit (dépend du type de recherche)
     */
    virtual void addChild(std::vector<Node> &liste, Node childNode);

    /**
     * Ajoute une liste de paires d'Action-State en tant qu'enfants du noeud courant
     * @param liste  La liste de noeuds à explorer
     * @param parent  Le Node qui sera parent de tous les nouveaux Nodes
     * @param children  Une liste de paires Action-State qui seront transformées en Node et ajoutées à la liste
     */
    void addChildrenNodes(std::vector<Node> &liste, Node *parent, std::vector<std::pair<Action, State> > children);

    /**
     * Remonte depuis un Node jusqu'au début et construit un chemin
     */
    Path* constructPath(Node *node, int cost=0);

    /**
     * Teste si l'état donné est un état final
     * @param state  L'état à tester
     */
    bool isGoal(State state);

    /**
     * Retourne la valeur de l'heuristique pour l'état
     */
    double heuristique(State state);

    /**
     * Lance la recherche
     * @return Le chemin du début au résultat, ou null si aucun chemin n'est trouvé
     */
    Path* search();

  /**
   * Détermine si un état est valide ou non (aka si le perso est pas dans un mur)
   */
    bool stateValid(State state);

    int getNodesVisited();

    void setTypeHeuristique(int type);
};
