#pragma once

#include <vector>
#include <string>

#include "vec2i.hpp"


class Labyrinthe{

    private:

        /*****************/
        /* Attributs     */
        /*****************/

        /**
        *   Tableau à deux dimensions contenant la description du labyrinthe
        *   selon les valeurs suivantes:
        *   -1 : sortie
        *   0 : couloir empruntable
        *   1 : mur non-franchissable
        *   2 : position de départ du joueur (et couloir empruntable)
        */
        std::vector<std::vector<int> > map;

        /**
        *   Position du point de départ du joueur dans le labyrinthe selon
        *   les coordonnées (x,y)
        */
        vec2i start_pos;

        /**
        *   Positions des différentes sorties du labyrinthe selon leurs
        *   coordonnées (x,y) stockées dans un vecteur
        */
        std::vector<vec2i> exits_pos;

    public:

        /*****************/
        /* Constructeurs */
        /*****************/

        /**
        *   Constructeur prenant en paramètre un vecteur à deux dimensions d'entiers représentant le labyrinthe selon les contraintes suivantes : 
        *   - doit contenir au moins deux cases avec une sortie et un point de départ du joueur
        *   - les valeurs autorisées sont comprises entre -1 et 2 inclus
        *   - les lignes et colonnes doivent avoir respectivement la même taille
        *   - il ne peut y avoir plus d'un point de départ
        *   Dans le cas contraire une exception est levée
        *   @param map    le tableau à deux dimensions d'entiers passé par référence
        */
        Labyrinthe(std::vector<std::vector<int> > & map);

        /**
        *   Constructeur prenant en paramètre un tableau à deux dimensions d'entiers, le nombre de lignes et colonnes de ce dernier selon les contraintes suivantes : 
        *   - doit contenir au moins deux cases avec une sortie et un point de départ du joueur
        *   - les valeurs autorisées sont comprises entre -1 et 2 inclus
        *   - les lignes et colonnes doivent avoir respectivement la même taille@param width et @param height
        *   - il ne peut y avoir plus d'un point de départ
        *   @param map    le tableau d'entiers statique à deux dimensions
        *   @param width  le nombre de colonnes
        *   @param height le nombre de lignes
        */
        Labyrinthe(int** map, int width, int height);

        /**
        *   Destructeur de la classe Labyrinthe
        *   (actuellement non utile car pas d'utilisation de pointeur(s)
        *   comme attribut(s) )
        */
        ~Labyrinthe();

        /*****************/
        /* Getters       */
        /*****************/

        /**
        *   Fonction retournant un descriptif de la map du labyrinthe
        *   @return     un vecteur à deux dimensions d'entiers représentant la
        *   map
        **/
        const std::vector<std::vector<int> > get_map() const;

        /**
        *   Fonction retournant les positions des différentes sorties du labyrinthe aux coordonnées (x,y) correspondantes
        *   @return     un vecteur de vec2 d'entiers contenant les positions x et y des sorties
        */
        const std::vector<vec2i> & get_exits() const;

        /**
        *   Fonction retournant la position de départ du joueur dans le labyrinthe aux coordonnées (x,y)
        *   @return     un vec2 d'entiers contenant les positions x t y du joueur
        */
        vec2i get_start_pos() const;

        /**
        *   Fonction retournant les dimensions du labyrinthe (les dimensions du tableau à deux dimensions d'entiers
        *   représentant la map
        *   @return     un vec2 d'entiers (largeur / nombre de colonnes, hauteur / nombre de lignes)
        */
        vec2i get_map_dims() const;

        /**
        *   Fonction retournant la valeur à une position passée en paramètre. Si celle ci n'est pas dans les limites de la map
        *   elle retourne une exception
        *   @param pos  le vecteur d'entiers donnant les coordonnées (x,y) de la position dont on veut récupérer la valeur
        *   @return     la valeur comprise entre -1 et 2 incluse
        */
        int get_value(vec2i pos) const;

        /*****************/
        /* Setters       */
        /*****************/

        /**
        *   Fonction permettant de modifier une ligne existante selon les contraintes suivantes:
        *   - la taille de la ligne ne peut dépasser la largeur de la map
        *   - la ligne ne peut enlever une sortie si celle ci est unique dans le labyrinthe
        *   - le numéro de ligne à modifier doit être 0 <= @param n_row < nombre de lignes de la map
        *   - la modification de la position de départ du joueur (valeur à 2) entraine automatiquement la suppression de l'ancienne (transformation en couloir)
        *   Dans le cas contraire une exception est levée
        *   @param new_row    un vecteur d'entiers représentant les modifications de la ligne
        *   @param n_row      le numéro de la ligne à modifier
        */
        void modify_row(std::vector<int> & new_row, int n_row);

        /**
        *   Fonction permettant de modifier une colonne existante selon les contraintes suivantes:
        *   - la taille de la colonne ne peut dépasser la hauteur de la map
        *   - la colonne ne peut enlever une sortie si celle ci est unique dans le labyrinthe
        *   - le numéro de colonne à modifier doit être 0 <= @param n_col < nombre de colonnes de la map
        *   - la modification de la position de départ du joueur (valeur à 2) entraine automatiquement la suppression de l'ancienne (transformation en couloir)
        *   Dans le cas contraire une exception est levée
        *   @param new_col    un vecteur d'entiers représentant les modifications de la colonne
        *   @param n_col      le numéro de la colonne à modifier
        */
        void modify_col(std::vector<int> & new_col, int n_col);

        /**
        *   Fonction permettant de modifier la valeur d'une case de la map aux coordonnées @param pos selon
        *   les contraintes suivantes:
        *   - la position doit être comprise 0 < pos < dimensions de la map
        *   - la modification ne peut supprimée une sortie (valeur à -1) si celle ci est unique dans le labyrinthe
        *   - la modification de la position de départ du joueur (valeur à 2) entraine automatiquement la suppression de l'ancienne (transformation en couloir)
        *   sur la map
        *   - la valeur @param value doit être uniquement une sortie (-1), un couloir (0), un mur (1) ou une position de départ (2)
        *   Dans le cas contraire une exception est levée
        *   @param pos    un vec2 d'entiers représentant la position (x,y) où la modification est appliquée 
        *   @param value  la valeur de modification à apporter aux coordonnées passées en paramètres
        */
        void modify_pos(vec2i pos, int value);

        /**
        *   Fonction permettant de supprimer une ligne de la map du labyrinthe selon les contraintes suivantes:
        *   - la ligne ne peut enlever une sortie si celle ci est unique dans le labyrinthe
        *   - le numéro de ligne à modifier doit être 0 <= @param n_row < nombre de lignes de la map
        *   - on ne peut supprimer une ligne si il n'en reste qu'une au total
        *   - on ne peut supprimer une ligne si celle ci contient la position de départ du joueur
        *   Dans le cas contraire une exception est levée
        *   @param n_row  le numéro de ligne à supprimer
        */
        void delete_row(int n_row);

        /**
        *   Fonction permettant de supprimer une colonne de la map du labyrinthe selon les contraintes suivantes:
        *   - la colonne ne peut enlever une sortie si celle ci est unique dans le labyrinthe
        *   - le numéro de colonne à modifier doit être 0 <= @param n_col < nombre de colonnes de la map
        *   - on ne peut supprimer une colonne si il n'en reste qu'une au total
        *   - on ne peut supprimer une colonne si celle ci contient la position de départ du joueur
        *   Dans le cas contraire une exception est levée
        *   @param n_col  le numéro de colonne à supprimer
        */
        void delete_col(int n_col);

        /**
        *   Fonction permettant d'ajouter une nouvelle ligne à la map du labyrinthe à la coordonnée @param pos_y selon les contraintes suivantes:
        *   - la taille de la ligne ne peut dépasser la largeur de la map
        *   - la modification de la position de départ du joueur (valeur à 2) entraine automatiquement la suppression de l'ancienne
        *   Dans le cas contraire une exception est levée
        *   @param new_row    la nouvelle ligne à ajouter la map
        *   @param pos_y      la position en y où sera ajoutée la ligne
        */
        void add_row(std::vector<int> & new_row, int pos_y);
        
        /**
        *   Fonction permettant d'ajouter une nouvelle colonne à la map du labyrinthe à la coordonée @param pos_x selon les contraintes suivantes:
        *   - la taille de la colonne ne peut dépasser la hauteur de la map
        *   - la modification de la position de départ du joueur (valeur à 2) entraine automatiquement la suppression de l'ancienne
        *   Dans le cas contraire une exception est levée
        *   @param new_col    la nouvelle colonne à ajouter à la map
        *   @param pos_x      la position en x où sera ajoutée la colonne
        */
        void add_col(std::vector<int> & new_col, int pos_x);
        
        /*****************/
        /* Méthodes      */
        /*****************/


        /********************/
        /* Fonctions static */
        /********************/

        /**
        *   Fonction permettant d'importer et de créer une instance de Labyrinthe depuis un fichier texte au format csv
        *   (ce dernier utilise le séparateur ";") .
        *   @param pathfile   le nom du fichier contenant le descriptif de la map du labyrinthe
        *   @return     l'instance de Labyrinthe générée
        */
        static Labyrinthe import(std::string pathfile);

    /**
     * Fonction important un labyrinthe au format du prof
     * @param pathfile  le nom du fichier du labyrinthe
     * @return  l'instance du labyrinthe
     */
    static Labyrinthe importProf(std::string pathfile);
};

