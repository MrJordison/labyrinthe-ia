#pragma once

#include <SFML/Graphics.hpp>

#include "ObjectRender.hpp"
#include "Labyrinthe.hpp"

/**
*   Classe héritante d'ObjectRender gérant l'affichage 2D d'une instance de Labyrinthe
*/
class LabyrintheRender: public ObjectRender{

    private:

        /*****************/
        /* Attributs     */
        /*****************/

        /**
        *   Instance de Labyrinthe à afficher
        */
        Labyrinthe* model_laby;

        /**
        *   Dictionnaire contenant des Texture attribuée à chaque type de valeur dans la map du Labyrinthe
        */
        std::map<int,sf::Texture*>* textures;

        /**
        *   Dictionnaire contenant Les Sprite créés à partir des Texture attribuée à chaque type de valeur dans la map du Labyrinthe
        */
        std::map<int,sf::Sprite*>* sprites;

        /**
        *   attribut gérant le coefficient de taille des cases de textures à afficher.
        */
        float upscale;

    public:

        /*****************/
        /* Constructeurs */
        /*****************/

        /**
        *   Constructeur de LabyrintheRender prenant en paramètre une instance de Labyrinthe à afficher
        *   @param laby un pointeur sur une instance de Labyrinthe à afficher
        */
        LabyrintheRender(Labyrinthe * laby);

        /**
        *   Destructeur de LabyrintheRender
        */
        ~LabyrintheRender();

        /*****************/
        /* Getters       */
        /*****************/

        /**
        *   Fonction retournant le coefficient de taille des blocs de textures
        *   @return le coefficient en float
        */
        float get_upscale() const;

        /*****************/
        /* Setters       */
        /*****************/
        
        /**
        *   Fonction permettant de changer le coefficient de taille des blocs de textures. Lors du changement les textures
        *   sont également mises à jour.
        *   @param u le coefficient en float
        */
        void set_upscale(float u);

        /**
        *   Fonction permettant d'affecter une texture à un type d'élément du labyrinthe (mur, chemin, entrée, sortie...)
        *   @param value la valeur dans la map 2D correspondant à un type d'élément du labyrinthe
        *   @param filename le chemin d'accès au fichier image de la texture à appliquer
        */
        bool setTexture(int value, std::string filename);

        /*****************/
        /* Méthodes      */
        /*****************/

        /**
        *   Surcharge de la méthode init d'ObjectRender
        */
        virtual void init();
        
        /**
        *   Surcharge de la méthode render d'ObjectRender, prend en paramètre une instance de RenderWindow ou sera affichée la map du labyrinthe
        *   @param window la fenêtre ou sera affichée le labyrinthe
        */
        virtual void render(sf::RenderWindow& window);

        /**
        *   Surcharge de la méthode update d'ObjectRender
        */
        virtual void update();
    
};
