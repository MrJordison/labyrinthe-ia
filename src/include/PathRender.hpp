#pragma once

#include "Path.hpp"
#include "ObjectRender.hpp"

#include <vector>

class PathRender: public ObjectRender{

    private:
        Path* path;
        std::vector<vec2i>* positions;
        sf::Sprite* sprite;

        int cpt;

        int speed;

        float upscale;

    public:

        /**
        *   Constructeur de la classe PathRender. Prend en paramètre un pointeur vers une instance de Path à afficher
        *   à l'écran
        *   @param p l'instance de Path à afficher
        */
        PathRender(Path* p);

        /**
        *   Desructeur de la classe PathRender
        */
        ~PathRender();

        /**
        *   Fonction permettant de changer la texture utilisée pour tracer le chemin, prend en paramètre le chemin de l'image contenant
        *   la texture. Renvoie un boolean indiquant si la texture a bien été chargée
        *   @param filename le path vers le fichier image de la texture
        *   @return un boolean a true si la texture a été créée et chargée sans problème, sinon renvoie false
        */
        bool setTexture(std::string filename);

        /**
        *   Surcharge de la fonctio init d'ObjectRender
        */
        virtual void init();

        /**
        *   Surcharge de la fonction render d'ObjectRender. Prend en paramètre un référence de RenderWindow dans laquelle le path sera affiché
        *   @param window la référence vers la fenêtre dans laquelle le Path sera dessiné
        */
        virtual void render(sf::RenderWindow& window);

        /**
        *   Surcharge de la fonction update d'ObjectRender
        */
        virtual void update();

        bool set_path(Path * path);

        Path* get_path() const;

        void maj_compteur();

        void set_speed(int speed);
        int get_speed() const;

        void set_upscale(float u);
        float get_upscale() const;

};
