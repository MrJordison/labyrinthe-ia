#pragma once

#include <ostream>
#include "Direction.hpp"

class Action{
  
private:
  
    Direction direction;
    float cout;
    
public:
    Action();
    Action(Direction dir, float c);
    Action(const Action &copy);

    Direction getDirection() const;
    float getCout() const;
    
    /*****************/
    /* Méthodes      */
    /*****************/
    
    friend std::ostream& operator<<(std::ostream& os, const Action& action);

  
};
