#pragma once

#include "SearchProblem.hpp"

class SearchBestFirst : public SearchProblem {
public:
    SearchBestFirst(State &start, Labyrinthe *laby);
    void addChild(std::vector<Node> &liste, Node childNode);
};
