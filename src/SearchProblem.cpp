#include "include/SearchProblem.hpp"

SearchProblem::SearchProblem(State &start, Labyrinthe* laby) {
    this->startState = State(start);
    this->nodesVisited = 0;
    this->laby = laby;
    func_visite_noeud = nullptr;
    typeHeuristique = 1;
}

void SearchProblem::setNodeCallback(std::function<void(GrapheRender*, Node)> func, GrapheRender* g_render) {
    func_visite_noeud = func;
    obj_render = g_render;
}

bool SearchProblem::isGoal(State state) {
  try {
    if (laby->get_value(state.getPosition()) == -1) {
	return true;
    }
    return false;
  } catch (std::string const& error) {
    return false;
  }
}

double SearchProblem::heuristique(State state) {
    double distance = 99999;
    for (auto const& exit : laby->get_exits()) {
	if (typeHeuristique == 1) {
	    double manhattan =
		(state.getPosition().x - exit.x)
		* (state.getPosition().x - exit.x)
		+
		(state.getPosition().y - exit.y)
		* (state.getPosition().y - exit.y);
	    manhattan = sqrt(manhattan);

	    if (manhattan < distance) {
		distance = manhattan;
	    }
	} else {
	    double cases = abs(exit.x - state.getPosition().x)
		+ abs(exit.y - state.getPosition().y);

	    if (cases < distance) {
		distance = cases;
	    }
	}
    }

    return distance;
}

void SearchProblem::addChild(std::vector<Node> &liste, Node childNode) {
    // On implémente un parcours en largeur
    if (stateValid(childNode.getState())) {
	liste.push_back(Node(childNode));
    }
}

void SearchProblem::addChildrenNodes(std::vector<Node> &liste, Node *parent, std::vector<std::pair<Action, State> > children) {
    for(auto const& child : children) {
	Node node = Node(child.second, parent, child.first, parent->getCost() + 1, parent->getDepth() + 1);

	// On vérifie si on a déjà parcouru ce noeud
	bool dejaVu = false;
	for (auto const& n : alreadyVisited) {
	    if (n.getState() == child.second) {
		dejaVu = true;
		break;
	    }
	}

	if (!dejaVu) {
	    addChild(liste, node);
	    alreadyVisited.push_back(Node(node));
	    nodesVisited++;
	}
    }
}

int SearchProblem::getNodesVisited() { return nodesVisited; }

Path* SearchProblem::search() {
    std::vector<Node> liste;
    Node startNode = Node(startState, nullptr, Action(Direction::HAUT, 0), 0, 0);
    liste.push_back(startNode);

    while(!liste.empty()) {
	Node *current = new Node(liste.front());
	liste.erase(liste.begin());

	if (func_visite_noeud != nullptr) {
	    func_visite_noeud(obj_render, *current);
	}

	if (isGoal(current->getState())) {
	    return constructPath(current);
	}

	std::vector<std::pair<Action, State> > successeurs =
	    current->getState().successors();
	addChildrenNodes(liste, current, successeurs);
    }
    return nullptr;
}

bool SearchProblem::stateValid(State state) {
  try {
    if (laby->get_value(state.getPosition()) != 1) {
      return true;
    }
    return false;
  } catch (std::string const& error) {
    return false;
  }
}

Path* SearchProblem::constructPath(Node *node, int cost) {
    // std::cout << "constructPath: " << node->getDepth() << std::endl;
    if (node->getParent() == nullptr) {
	return new Path(node->getState(), cost);
    }

    Path *path = constructPath(node->getParent(), cost+1);
    path->addStep(std::make_pair(node->getAction(), node->getState()));
    
    return path;
}

void SearchProblem::setTypeHeuristique(int type) {
    this->typeHeuristique = type;
}
