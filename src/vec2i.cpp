#include "include/vec2i.hpp"

#include <iostream>

using namespace std;

/*****************/
/* Constructeurs */
/*****************/

vec2i::vec2i(int x, int y){
    this->x = x;
    this->y = y;
}

/*****************/
/* Méthodes      */
/*****************/

vec2i operator+(const vec2i& v, const vec2i& v2){
    return vec2i(v.x + v2.x, v.y + v2.y);
}

vec2i operator-(const vec2i& v, const vec2i& v2){
    return vec2i(v.x - v2.x, v.y - v2.y);
}

vec2i vec2i::operator+=(const vec2i& v){
    return vec2i(this->x + v.x, this->y + v.y);
}

vec2i vec2i::operator-=(const vec2i& v){
    return vec2i(this->x - v.x, this->y - v.y);
}

bool operator==(const vec2i& v, const vec2i& v2){
    return ((v.x == v2.x) && (v.y == v2.y));
}

bool operator<(const vec2i& v, const vec2i& v2){
    if(v.y < v2.y)
        return true;
    else if (v.y > v2.y)
        return false;
    else if(v.x < v2.x)
        return true;
    else
        return false;
}

std::ostream& operator<<(std::ostream& os, const vec2i& v){
    return os << "(" << v.x << "," << v.y << ")";    
}

