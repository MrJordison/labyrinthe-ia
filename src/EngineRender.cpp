#include "include/EngineRender.hpp"

using namespace std;


EngineRender::EngineRender(){
    objects = new vector<ObjectRender*>(); 
} 

EngineRender::EngineRender(const EngineRender& render){
    objects = new vector<ObjectRender*>();
    for(ObjectRender* object : *(render.objects))
        objects->push_back(new ObjectRender(*object));
}

EngineRender::~EngineRender(){
    for(ObjectRender* object : *objects)
        delete object;
    delete objects;
}

void EngineRender::init(){
    for(ObjectRender* object : *objects)
        object->init();
}

void EngineRender::render(sf::RenderWindow& window){
    window.clear(sf::Color::Black);

    this->update();

    for(ObjectRender* object : *objects)
        object->render(window);

    window.display();

}

void EngineRender::update(){
    for(ObjectRender* object : *objects)
        object->update();
}

const vector<ObjectRender*>& EngineRender::get_objects() const{
    return *objects;
}

void EngineRender::add_object(ObjectRender* obj){
    objects->push_back(obj);
}


EngineRender& EngineRender::operator=(const EngineRender& render){
    delete objects;
    objects = new vector<ObjectRender*>();
    for(ObjectRender* object : *(render.objects))
        objects->push_back(new ObjectRender(*object));
    return *this;
}
