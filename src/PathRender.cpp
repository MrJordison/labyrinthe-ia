#include "include/PathRender.hpp"

#include <thread>
#include <chrono>

using namespace std;

PathRender::PathRender(Path * p){
    path = nullptr;
    positions = nullptr;
    sprite = nullptr;
    set_path(p);
    cpt = 0;
    speed=1;
    upscale = 1.f;
}

PathRender::~PathRender(){
    delete path;
    delete positions;
}
void PathRender::maj_compteur(){
    this_thread::sleep_for(chrono::milliseconds(2000/speed));
        cpt++;
    if(cpt < positions->size()){
        thread bla{&PathRender::maj_compteur,this};
        bla.detach();
    }
}

void PathRender::init(){

}

void PathRender::render(sf::RenderWindow& window){
     if(positions!=nullptr){
        vector<vec2i>::iterator i;
        int nb_displayed = 0;
        for(i = positions->begin()  ; i != positions->end(); ++i){
            if(nb_displayed >= cpt){
                break;
            }
            if(sprite!=nullptr){
                sprite->setPosition((*i).x*32*upscale,(*i).y*32*upscale);
                window.draw(*sprite);
            }
            nb_displayed++;

        }
    }       
}

void PathRender::update(){
    cpt++;
}

bool PathRender::setTexture(string filename){   
    sf::Texture *  texture = new sf::Texture();
    if(!texture->loadFromFile(filename,sf::IntRect(0,0,32,32))){
        delete texture;
        return false;
    }
    sprite = new sf::Sprite();
    sprite->setTexture(*texture);
    sprite->setColor(sf::Color(255,255,255,128));

}

bool PathRender::set_path(Path * path){
    if(path == nullptr)
        return false;
    if(this->path != nullptr)
        delete this->path;
    if(positions!=nullptr)
        delete positions;
    this->path = path;
    positions =  new vector<vec2i>();
    if(this->path!=nullptr)
        for(pair<Action,State> pos : this->path->getListe())
            positions->push_back(pos.second.getPosition());
    cpt = 0;

    //appelle du timer callback pour l'incrémentation de cpt;
    thread bla{&PathRender::maj_compteur,this};
    bla.detach();

    return true;
}

Path * PathRender::get_path() const{
    return path;
}

void PathRender::set_speed(int speed){this->speed = speed;}

int PathRender::get_speed() const{ return speed;}

float PathRender::get_upscale() const{return upscale;}

void PathRender::set_upscale(float u){
    sprite->setScale((1.f/upscale)*u,(1.f/upscale)*u);
    this->upscale = u;
}
