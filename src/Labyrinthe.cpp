#include "include/Labyrinthe.hpp"

#include <iostream>

#include "include/Importer.hpp"

using namespace std;


Labyrinthe::Labyrinthe(std::vector<std::vector<int>> & map){
    //Test si la map n'est pas vide
    if(map.size() > 0){
        if(map.at(0).size() > 0){

            //initialisation des variables temporaires et de vérification
            int width;
            width = map.at(0).size();
            bool exit = false;
            bool start = false;
            int value;

            //Parcours de chaque ligne de la map
            for(int i = 0; i < map.size(); ++i){
                if(map.at(i).size() == width){
                    //Parcours de chaque colonne par ligne
                    for(int j = 0; j < map.at(i).size(); ++j){

                        //Récupération de la valeur à l'emplacement(j,i) (j abscisse, i ordonnée)
                        value = map.at(i).at(j);  

                        //Test si la valeur est une sortie
                        if(value ==-1){
                            exit = true;
                            //ajout de la position de sortie dans la liste des sorties
                            exits_pos.push_back(vec2i(j,i));
                        }

                        //Test si la valeur est un point de départ
                        else if(value==2){
                            //Test si il n'y a qu'un seul point de départ
                            if(!start){
                                start = true;
                                start_pos = vec2i(j,i);    
                            }
                            else
                                throw string("Il y a plus de deux points de départ\n");
                        }
                        //Test si la valeur est un mur ou un couloir
                        else if(value!=0 && value !=1)
                            throw string("Erreur valeur indiquée non valide\n");
                    }
                }
                else
                    throw string("Erreur dimensions de lignes différentes\n");
            }
            //Test si le labyrinthe contient un point de départ et au moins une sortie
            if(exit && start){
                //stockage de la map
                this->map = map;

                /*
                //suppression du point de départ dans la map, considéré comme un couloir
                //facilite les tests de visibilité
                //cependant stocké dans un attribut
                this->map.at(start_pos.y).at(start_pos.x) = 0;
                */
                }
            else
                throw string("Erreur ne contient pas de sortie ou de point de départ\n");
        }
        else
            throw string("Erreur tableau vide\n");
    }
    else
        throw string("Erreur tableau vide\n");
}

Labyrinthe::Labyrinthe(int** map, int width, int height){
    //Test si la map n'est pas vide
    if(width > 0 && height > 0){

        this->map = vector<vector<int>>();
        //initialisation des variables temporaires et de vérification
        int width;
        bool exit = false;
        bool start = false;
        int value;

        //Parcours de chaque ligne de la map
        for(int i = 0; i < height; ++i){
            
            this->map.push_back(vector<int>());

            //Parcours de chaque colonne par ligne
            for(int j = 0; j < width; ++j){
                
                //Récupération de la valeur à l'emplacement(i,j) (j abscisse, i ordonnée)
                value = map[i][j];  

                this->map.at(i).push_back(value);

                //Test si la valeur est une sortie
                if(value ==-1){
                    exit = true;
                    //ajout de la position de sortie dans la liste des sorties
                    exits_pos.push_back(vec2i(j,i));
                }

                //Test si la valeur est un point de départ
                else if(value==2){
                    //Test si il n'y a qu'un seul point de départ
                    if(!start){
                        start = true;
                        start_pos = vec2i(j,i);    
                    }
                    else
                        throw string("Il y a plus de deux points de départ\n");
                }
                //Test si la valeur est un mur ou un couloir
                else if(value!=0 && value !=1)
                    throw string("Erreur valeur indiquée non valide\n");
            }
        }
        //Test si le labyrinthe contient un point de départ et au moins une sortie
        if(exit && start){
        /*//suppression du point de départ dans la map, considéré comme un couloir
        //facilite les tests de visibilité
        //cependant stocké dans un attribut
        this->map.at(start_pos.x).at(start_pos.y) = 0;
        */
        }
        else
            throw string("Erreur ne contient pas de sortie ou de point de départ\n");

    }
    else
        throw string("Erreur tableau vide\n");
}

Labyrinthe::~Labyrinthe(){
    
}

const std::vector<std::vector<int>> Labyrinthe::get_map() const{
   return map; 
}

const std::vector<vec2i> & Labyrinthe::get_exits() const{
   return exits_pos; 
}

vec2i Labyrinthe::get_start_pos() const{
   return start_pos; 
}

vec2i Labyrinthe::get_map_dims() const{
   return vec2i(map.at(0).size(),
		map.size()); 
}

int Labyrinthe::get_value(vec2i pos) const{
    if(pos.x <0 || pos.x >=get_map_dims().x || pos.y<0 || pos.y >=get_map_dims().y)
        throw string("Coordonnées hors limites de la map");
    return map.at(pos.y).at(pos.x);
}

void Labyrinthe::modify_row(std::vector<int> & new_row, int n_row){
   //Test taille de la ligne
   if(new_row.size() != get_map_dims().x)
        throw string("La taille de la ligne est différente de la largeur de la map\n");
    //Test indice dans les limites de la map
    else if(n_row >= get_map_dims().x || n_row<0)
        throw string("L'indice de la ligne à modifier est incorrect");

    //Init valeurs intermédiaire
    bool new_start = false;
    vec2i new_start_pos;
    bool has_exit, had_exit;
    has_exit = had_exit = false;
    int old_x, new_x;

    //Tests valeurs valides
    for(int i = 0 ; i < new_row.size(); ++i){
        if(new_row.at(i) ==2){
            new_start = true;
            new_start_pos = vec2i(i,n_row);
        }
        else if(new_row.at(i) == -1){
            has_exit = true;
            new_x = i;
        }
        else if(new_row.at(i) != 0 && new_row.at(i) !=1)
            throw string("La valeur de la ligne est invalide");

        //Vérification sortie existante dans l'ancienne ligne
        if(map.at(n_row).at(i) == -1){
            had_exit = true;
            old_x = i;
        }
    }
    
    //Test si suppression d'une sortie avec modification
    //et si oui si celle ci est unique dans le labyrinthe
    if(had_exit){
        if(!has_exit && exits_pos.size()==1)
            throw string("Il n'est pas possible de supprimer une sortie si celle ci est unique dans le labyrinthe");
        else{
            //Suppression de la sortie de la liste
            for(int j = 0; j < exits_pos.size(); ++j){
                if(exits_pos.at(j).x == old_x && exits_pos.at(j).y == n_row){
                    exits_pos.erase(exits_pos.begin() + j);
                    break;
                }
            }
        }
    }
    //Cas nouvelle sortie
    if(has_exit){
        exits_pos.push_back(vec2i(new_x,n_row));
    }
    
    //Test si nouveau point de départ
    if(new_start){
        map.at(start_pos.y).at(start_pos.x) = 0;
        start_pos = new_start;
    }

    //Si tous les tests passés, alors modification de la map
    for(int i = 0; i < get_map_dims().x ; ++i)
        map.at(n_row).at(i) = new_row.at(i);
}

void Labyrinthe::modify_col(std::vector<int> & new_col, int n_col){
   //Test taille de la colonne
   if(new_col.size() != get_map_dims().y)
        throw string("La taille de la colonne est différente de la hauteur de la map\n");
    //Test indice dans les limites de la map
    else if(n_col >= get_map_dims().y || n_col<0)
        throw string("L'indice de la colonne à modifier est incorrect");

    //Init valeurs intermédiaire
    bool new_start = false;
    vec2i new_start_pos;
    bool has_exit, had_exit;
    has_exit = had_exit = false;
    int old_y, new_y;

    //Tests valeurs valides
    for(int i = 0 ; i < new_col.size(); ++i){
        if(new_col.at(i) ==2){
            new_start = true;
            new_start_pos = vec2i(n_col,i);
        }
        else if(new_col.at(i) == -1){
            has_exit = true;
            new_y = i;
        }
        else if(new_col.at(i) != 0 && new_col.at(i) !=1)
            throw string("La valeur de la colonne est invalide");

        //Vérification sortie existante dans l'ancienne colonne
        if(map.at(i).at(n_col) == -1){
            had_exit = true;
            old_y = i;
        }
    }
    
    //Test si suppression d'une sortie avec modification
    //et si oui si celle ci est unique dans le labyrinthe
    if(had_exit){
        if(!has_exit && exits_pos.size()==1)
            throw string("Il n'est pas possible de supprimer une sortie si celle ci est unique dans le labyrinthe");
        else{
            //Suppression de la sortie de la liste
            for(int j = 0; j < exits_pos.size(); ++j){
                if(exits_pos.at(j).y == old_y && exits_pos.at(j).x == n_col){
                    exits_pos.erase(exits_pos.begin() + j);
                    break;
                }
            }
        }
    }
    //Cas nouvelle sortie
    if(has_exit){
        exits_pos.push_back(vec2i(n_col,new_y));
    }
    
    //Test si nouveau point de départ
    if(new_start){
        //Changement ancien point de départ en couloir
        map.at(start_pos.y).at(start_pos.x) = 0;
        start_pos = new_start;
    }

    //Si tous les tests passés, alors modification de la map
    for(int i = 0; i < get_map_dims().y ; ++i)
        map.at(i).at(n_col) = new_col.at(i);
}

void Labyrinthe::modify_pos(vec2i pos, int value){
    //Test coordonnées position
    if(pos.x <0 || pos.x >=get_map_dims().x || pos.y<0 || pos.y >=get_map_dims().y)
        throw string("La position indiquée est hors limite de la map");
    
    //Test ancienne valeur à modifier
    //Cas suppression sortie
    if(map.at(pos.y).at(pos.x) ==-1){
        //Test suppression sortie unique
        if(value !=-1 && exits_pos.size()==1)
            throw string("Il n'est pas possible de supprimer une sortie si celle ci est unique dans le labyrinthe");
    }
    //cas suppression point de départ
    else if(map.at(pos.y).at(pos.x)==2 && value !=2)
        throw string("Il n'est pas possible de supprimer le point de départ");
    //Sinon vérification et modification selon nouvelle valeur
    else{
        if(value == -1 && map.at(pos.y).at(pos.x)!=1){
            map.at(pos.y).at(pos.x)==value;
        }
        else if(value == 2){
            map.at(start_pos.y).at(start_pos.x)=0;
            start_pos = pos;
        }
        //Cas valeur non valide
        else if(value !=0 && value !=0)
            throw string("Nouvelle valeur non valide");
        
        //Application nouvelle valeur at pos
        exits_pos.push_back(pos);
    }
}

void Labyrinthe::delete_row(int n_row){
    //Test si une seule ligne restante
    if(get_map_dims().y ==1)
        throw string("Vous ne pouvez pas supprimer la dernière ligne");
    else{
        vector<vec2i> exits_to_del = vector<vec2i>();
        int cpt_exits = exits_pos.size();
        for(int i = 0 ; i < map.at(n_row).size() ; ++i){
            //Test suppression point de départ
            if(map.at(n_row).at(i) == 2)
                throw string("Vous ne pouvez pas supprimer le point de départ");
            //Test si suppression d'une sortie unique
            else if(map.at(n_row).at(i)==-1){
                if(cpt_exits == 1)
                    throw string("Il n'est pas possible de supprimer une sortie si celle ci est unique dans le labyrinthe");
                else{
                    cpt_exits--;
                    exits_to_del.push_back(vec2i(i,n_row));
                }
            }
        }
        //Suppression de la ligne
        map.erase(map.begin()+n_row);
        //Suppression des positions de sorties de la liste
        for(vec2i del : exits_to_del)
            for(int i = 0 ; i < exits_pos.size(); ++i)
                if(del==exits_pos.at(i))
                    exits_pos.erase(exits_pos.begin()+i);
    }
}

void Labyrinthe::delete_col(int n_col){
    //Test si une seule colonne restante
    if(get_map_dims().x ==1)
        throw string("Vous ne pouvez pas supprimer la dernière colonne");
    else{
        vector<vec2i> exits_to_del = vector<vec2i>();
        int cpt_exits = exits_pos.size();
        for(int i = 0 ; i < map.size() ; ++i){
            //Test suppression point de départ
            if(map.at(i).at(n_col) == 2)
                throw string("Vous ne pouvez pas supprimer le point de départ");
            //Test si suppression d'une sortie unique
            else if(map.at(i).at(n_col)==-1){
                if(cpt_exits == 1)
                    throw string("Il n'est pas possible de supprimer une sortie si celle ci est unique dans le labyrinthe");
                else{
                    cpt_exits--;
                    exits_to_del.push_back(vec2i(n_col,i));
                }
            }
        }
        //Suppression de la colonne
        for(int i = 0 ; i < map.size(); ++i)
            map.at(i).erase(map.at(i).begin()+n_col);
        //Suppression des positions de sorties de la liste
        for(vec2i del : exits_to_del)
            for(int i = 0 ; i < exits_pos.size(); ++i)
                if(del==exits_pos.at(i))
                    exits_pos.erase(exits_pos.begin()+i);
    }
    
}

void Labyrinthe::add_row(std::vector<int> & new_row, int pos_y){
    //Test si position hors limite
    if(pos_y<0 || pos_y >= get_map_dims().y)
        throw string("La position indiquée est hors limite de la map");
    //Test si taille de la ligne différente de la largeur de la map
    else if(new_row.size() != get_map_dims().x)
        throw string("La taille de la ligne est différente de la largeur de la map\n");

    //Ligne valide, ajout à la map
    map.insert(map.begin()+pos_y, new_row);

    //maj selon valeurs
    for(int i = 0 ; i < new_row.size(); ++i){
        //Ajout des nouvelles sorties si il y a dans la liste
        if(new_row.at(i) == -1)
            exits_pos.push_back(vec2i(i,pos_y));

        //Si nouvelle position de départ, suppression de l'ancienne
        //Et modification vecteur position attribut
        else if(new_row.at(i) == 2){
            map.at(start_pos.y).at(start_pos.x) = 0;
            start_pos = vec2i(i,pos_y);
        }
    }
}

void Labyrinthe::add_col(std::vector<int> & new_col, int pos_x){
    //Test si position hors limite
    if(pos_x < 0 || pos_x >= get_map_dims().x)
        throw string("La position indiquée est hors limite de la map");
    //Test si taille de la colonne différente de la largeur de la map
    else if(new_col.size() != get_map_dims().y)
        throw string("La taille de la colonne est différente de la hauteur de la map\n");

    //Colonne valide, ajout à la map
    // et maj selon valeurs
    for(int i = 0 ; i < new_col.size(); ++i){
        //Ajout des nouvelles sorties si il y a dans la liste
        if(new_col.at(i) == -1)
            exits_pos.push_back(vec2i(pos_x,i));

        //Si nouvelle position de départ, suppression de l'ancienne
        //Et modification vecteur position attribut
        else if(new_col.at(i) == 2){
            map.at(start_pos.y).at(start_pos.x) = 0;
            start_pos = vec2i(pos_x,i);
        }
        
        //ajout de la valeur à l'indice pos_x dans la ligne i en cours
        map.at(i).insert(map.at(i).begin()+pos_x,new_col.at(i));
    }
}

Labyrinthe Labyrinthe::import(std::string pathfile){
    //Création importeur
    Importer* import = new Importer(pathfile);
    
    //Si le fichier n'existe pas, renvoie une exception
    if(!import->get_file())
        throw string("Le fichier est introuvable\n");

    vector<vector<int>> map = vector<vector<int>>();
    string contenu("");
    vector<string> line_split;
    int cpt = 0;
    //Boucle récupérant une ligne du fichier à la fois
    while(getline(import->get_file(),contenu)){
        //split de la ligne selon le séparateur
        line_split = Importer::split(contenu,';');
        
        //gestion cas ou ligne à vide dans fichier
        if(line_split.size()==0)
            break;

        //création d'une nouvelle ligne sur la map et remplissage
        map.push_back(vector<int>());
        for(int i = 0 ; i < line_split.size(); ++i)
            map.at(cpt).push_back(stoi(line_split.at(i)));

        cpt++;
    }

    delete import;

    return Labyrinthe(map);
}

Labyrinthe Labyrinthe::importProf(std::string pathfile) {
    Importer *import = new Importer(pathfile);

    if (!import->get_file()) {
	throw string("Le fichier est introuvable !\n");
    } 
    
    string contenu;

    getline(import->get_file(), contenu);
    int debut_x = stoi(contenu);

    getline(import->get_file(), contenu);
    int debut_y = stoi(contenu);

    getline(import->get_file(), contenu);
    int width = stoi(contenu);

    getline(import->get_file(), contenu);
    int height = stoi(contenu);
    
    int cpt = 0;
    
    vector<vector<int> > map;
    while(getline(import->get_file(), contenu)) {
	map.push_back(vector<int>());
	bool is_exit = false; // On garde un booléen car la sortie (-1) est sur deux cases -_-
	for (int i=0; i < contenu.length(); i++) {
	    char c = contenu.at(i);
	    if (c == ' ') continue;
	    if (!is_exit) {
		if (c == '1') {
		    map.at(cpt).push_back(1);
		} else if (c == '0') {
		    map.at(cpt).push_back(0);
		} else if (c == '-') {
		    map.at(cpt).push_back(-1);
		    is_exit = true;
		}
	    } else {
		if (c != '1') {
		    throw string("Erreur de format du labyrinthe !\n");
		} else {
		    is_exit = false;
		}
	    }
	}
	cpt++;
    }

    map.at(debut_y).at(debut_x) = 2;
    delete import;

    return Labyrinthe(map);
}
