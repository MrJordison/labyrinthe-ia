#include "include/SearchUniformCost.hpp"

SearchUniformCost::SearchUniformCost(State &start, Labyrinthe *laby) :
    SearchProblem::SearchProblem(start, laby) {
}

void SearchUniformCost::addChild(std::vector<Node> &liste, Node childNode) {
    if (SearchProblem::stateValid(childNode.getState())) {
	int g = childNode.getCost(); // Coût depuis le début

	int idx = 0;
	for(int i=0; i < liste.size(); i++) {
	    int gCourant = liste.at(idx).getCost();
	    if (gCourant > g) {
		break;
	    }
	}

	liste.insert(liste.begin() + idx, childNode);
    }
}
