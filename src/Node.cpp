#include "include/Node.hpp"

Node::Node(State state, Node* parent, Action action, double cost, int depth) {
    this->state = State(state);
    this->parent = parent;
    this->action = Action(action);
    this->cost = cost;
    this->depth = depth;
}

Node::Node(const Node &copy) {
    this->state = State(copy.getState());
    this->parent = copy.getParent();
    this->action = Action(copy.getAction());
    this->cost = copy.getCost();
    this->depth = copy.getDepth();
}

int Node::getCost() const { return cost; }

int Node::getDepth() const { return depth; }

Action Node::getAction() const { return action; }

Node* Node::getParent() const { return parent; }

State Node::getState() const { return state; }
